<?php

/**
 * @file
 * Devel addition functionality.
 */

/**
 * Implementation of hook_perm().
 *
 * @return array
 */
function devel_addition_perm() {
  return array('access devel addition information');
}

/**
 * Implementation of hook_menu().
 *
 * @return array
 */
function devel_addition_menu() {
  $items = array();

  $items['devel/form/%'] = array(
    'title' => 'Devel addition Form test',
    'description' => 'Devel addition Form testing page',
    'page callback' => 'devel_addition_render_form',
    'page arguments' => array(2),
    'access arguments' => array('access devel addition information'),
    'type' => MENU_CALLBACK,
  );

  // for a url ending in mymodule/myblock it will render the block called
  // myblock defined in module mymodule
  $items['devel/block/%/%'] = array(
    'title' => 'Devel addition Block test',
    'description' => 'Devel addition Block test page',
    'page callback' => 'devel_addition_render_block',
    'page arguments' => array(2, 3),
    'access arguments' => array('access devel addition information'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Return a rendered form.
 *
 * @param string $form_name
 * @return array|null|string
 */
function devel_addition_render_form($form_name) {
  return drupal_get_form($form_name);
}

/**
 * Return a rendered block.
 *
 * @param $module_name
 * @param $delta
 * @return mixed
 */
function devel_addition_render_block($module_name, $delta) {
  $block = module_invoke($module_name, 'block', 'view', $delta);

  // must be converted to an object
  $block = !empty($block) ? (object)$block : new stdclass;

  $block->module = $module_name;
  $block->delta = $delta;
  $block->region = 'dev';

  return theme('block', $block);
}
